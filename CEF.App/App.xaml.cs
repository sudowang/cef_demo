﻿using System;
using System.IO;
using System.Windows;
using CefSharp;
using CefSharp.Wpf;

namespace CEF.App;

/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : Application
{
    public App()
    {
        var settings = new CefSettings()
        {
            //By default CefSharp will use an in-memory cache, you need to specify a Cache Folder to persist data
            CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CefSharp\\Cache")
        };
        settings.CefCommandLineArgs.Add("enable-media-stream");
        //https://peter.sh/experiments/chromium-command-line-switches/#use-fake-ui-for-media-stream
        settings.CefCommandLineArgs.Add("use-fake-ui-for-media-stream");
        //For screen sharing add (see https://bitbucket.org/chromiumembedded/cef/issues/2582/allow-run-time-handling-of-media-access#comment-58677180)
        settings.CefCommandLineArgs.Add("enable-usermedia-screen-capturing");
        if (!Cef.IsInitialized)
        {
            //Perform dependency check to make sure all relevant resources are in our output directory.
            Cef.Initialize(settings, performDependencyCheck: true, browserProcessHandler: null);
        }
    }
}