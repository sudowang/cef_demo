# 如何使用
## 依赖
1. NodeJs，最新版即可
2. .net 6.0 SDK

## 启动
1. 启动CEF.Web
2. 在CEF.UI的cmd下
    + `npm install`
    + 启动`npm run start`
    + 程序启动后，默认运行在3000端口，可以支持hotreload

## 调试
1. 打开网页 http://localhost:3000 ，就可以查看界面
2. 打开 http://localhost:5252/swagger/index.html ，就可以查看后端API
3. F5运行CEF.APP就可以查看最终效果