﻿using CEF.Web.DataBase;
using Microsoft.AspNetCore.Mvc;

namespace CEF.Web.Controllers;

public class CityController : ControllerBase
{
    private readonly MetroDataContext _db;

    public CityController(MetroDataContext db)
    {
        _db = db;
    }

    [HttpGet("/api/cities")]
    public IActionResult GetCities()
    {
        return Ok(_db.Cities.Select(c => c).AsEnumerable());
    }

    [HttpGet("/api/lines")]
    public IActionResult GetLines()
    {
        return Ok(_db.Lines.AsEnumerable().Select((x, index) => new Line
        {
            Id = index,
            LineName = x.LineName,
            CityCode = x.CityCode
        }));
    }
}