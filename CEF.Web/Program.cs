using CEF.Web.DataBase;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
// ACCESS connection string configuration

builder.Services.AddScoped<MetroDataContext>();
const string reactAppCorsPolicy = "_reactAPPCorsPolicy";
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: reactAppCorsPolicy,
        corsPolicyBuilder => { corsPolicyBuilder.WithOrigins("http://localhost:3000"); }); // this is the url of the react app
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(reactAppCorsPolicy);

app.UseAuthorization();

app.MapControllers();

app.Run();