﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CEF.Web.DataBase;

[Table("Cities")]
public class City
{
    [Key] [Column("cn_name")] public string CityName { get; set; }
    [Column("cename")] public string CityPyName { get; set; }
}