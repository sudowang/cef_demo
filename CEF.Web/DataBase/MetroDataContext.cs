﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace CEF.Web.DataBase;

public class MetroDataContext : DbContext
{
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var fileName = Assembly.GetEntryAssembly()?.Location; // get the path of the executable web app
        var accdbPath = Path.Join(Path.GetDirectoryName(fileName), "database.accdb"); // get MS Access database path
        var connectionString = $@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={accdbPath};";
        optionsBuilder.UseJet(connectionString);
    }

    public DbSet<City> Cities { get; set; }
    public DbSet<Line> Lines { get; set; }
}