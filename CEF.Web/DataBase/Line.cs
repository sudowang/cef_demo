﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CEF.Web.DataBase;

[Table("Lines")]
public class Line
{
    [Key] [Column("line_name")] public string LineName { get; set; }

    [Column("city_code")] public int CityCode { get; set; }

    [NotMapped] public int Id { get; set; }
}