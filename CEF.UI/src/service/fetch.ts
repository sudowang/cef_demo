import { City } from '../model/City'
import axios from 'axios'
import { Line } from '../model/Line'


export default function () {
    return axios.get('http://localhost:5252/api/cities').then(x => x.data as City[]);
}


export function getLines() {
    return axios.get('http://localhost:5252/api/lines').then(x => x.data as Line[]);
}