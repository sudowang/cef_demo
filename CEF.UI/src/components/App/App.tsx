import React, { useEffect, useState } from 'react';
import './App.css';
import { getLines } from '../../service/fetch'
import LineTable from '../LineTable'
import { Button, Calendar, Tabs, Upload, version } from "antd";
import { UploadChangeParam } from 'antd/es/upload'
import { Line } from '../../model/Line'

const { TabPane } = Tabs;

function useLines() {
    const [lines, setLines] = useState<Line[]>([]);
    useEffect(() => {
        getLines().then(data => {
            setLines(data);
        })
    }, []);
    return lines;
}

function App() {
    const lines = useLines();

    function onFileUploadChange(fileInfo: UploadChangeParam) {
        console.log('ok')
    }

    return <div className="App">
        <Tabs defaultActiveKey="1">
            <TabPane tab="Tab 1" key="1">
                <h1>antd version: {version}</h1>
                <Upload onChange={onFileUploadChange}>
                    <Button loading={false}>
                        上传Excel文件
                    </Button>
                </Upload>
                <br />
                <br />
                <LineTable dataSource={lines} loading={!lines.length} />
            </TabPane>
            <TabPane tab="Tab 2" key="2">
                <Calendar />
            </TabPane>
            <TabPane tab="Tab 3" key="3">
                Content of Tab Pane 3
            </TabPane>
        </Tabs>
    </div>
}

export default App;
