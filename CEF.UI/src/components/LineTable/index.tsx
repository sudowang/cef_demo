import { Button, Table } from 'antd'
import React from 'react'
import { ColumnsType, } from 'antd/es/table';
import { Line } from '../../model/Line'

const columns: ColumnsType<Line> = [
    {
        title: '线路名称',
        dataIndex: 'lineName',
    },
    {
        title: '城市代码',
        dataIndex: 'cityCode',
    },
    {
        title: '操作',
        render: x => {
            return <Button type="link">删除</Button>
        }
    }
];

export const CityTable: React.FC<{
    dataSource: Line[]
    loading: boolean
}> = ({ dataSource, loading }) => {
    return (
        <Table
            pagination={false}
            dataSource={dataSource}
            columns={columns}
            loading={loading}
            rowKey={(record: Line) => record.id}
        />
    )
}
export default CityTable
